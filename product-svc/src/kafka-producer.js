const inversify = require('inversify')
const Kafka = require('node-rdkafka')

const TYPES = require('./types')

module.exports = ({container}) => {
  const { address } = container.get(TYPES.Config).kafka

  const producer = Kafka.Producer({
    'metadata.broker.list': address,
    'queue.buffering.max.ms': 10,
    'queue.buffering.max.messages': 1,
    'message.timeout.ms': 1,
    'event_cb': true,
    'dr_cb': true,
    // debug: 'all'
  })

  producer.on('event.log', e => console.log(`producer: event log: ${JSON.stringify(e)}`))

  //producer.on('ready', () => console.log('producer: I am connected'))
  producer.on('event.error', err => console.log(`producer: ERROR ${err}`))

  //producer.once('delivery-report', (err, report) => console.log('producer: report', report))

  producer.connect()

  return producer
}
