const inversify = require('inversify')
const uuid = require('uuid')
const TYPES = require('./types')

const { InvalidDataError } = require('./errors')

class ProductController {
  constructor(db) {
    this.db = db
  }

  async createProduct(data) {
    if (!data.title) {
      // Interestingly we could decide to shove it in the queue as is and _then_ run the validation.
      // This would required other validation (that would be there in the current format too) such as max message size.
      throw new InvalidDataError()
    }

    data._id = uuid.v4()
    await this.db.productCreated(data)
  }
}

inversify.decorate(inversify.injectable(), ProductController)
inversify.decorate(inversify.inject(TYPES.ProductDB), ProductController, 0);

module.exports = ProductController
