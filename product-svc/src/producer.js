const inversify = require('inversify')
const { promisify } = require('es6-promisify')
const TYPES = require('./types')


class ProductProducer {
  constructor(producer, topic) {
    this.producer = producer
    this.topic = topic
    this.flush = promisify(cb => producer.flush(2000, cb))
  }

  async productCreated(product) {
    this.producer.produce(this.topic, null, Buffer.from(JSON.stringify(product)))
    this.producer.poll()
      
    return this.flush()
  }
}

inversify.decorate(inversify.injectable(), ProductProducer)
inversify.decorate(inversify.inject(TYPES.KafkaProducer), ProductProducer, 0);

module.exports = {
  factory: ({container}) => {
    const kafkaProducer = container.get(TYPES.KafkaProducer)
    return ({ TOPIC }) => new ProductProducer(kafkaProducer, TOPIC)
  },
  ProductProducer
}

