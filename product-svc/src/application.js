const Koa = require('koa');
const koaBody = require('koa-body')
const Router = require('koa-better-router')

const TYPES = require('./types')
const { InvalidDataError } = require('./errors')

const app = new Koa();

async function errorHandler({response}, next) {
  try {
    await next()
  } catch (err) {
    if (err instanceof InvalidDataError) {
      response.status = 400
    } else {
      throw err
    }
  }
}

module.exports = ({container}) => {
  const productController = container.get(TYPES.ProductController)
  const api = Router({prefix: '/api'}).loadMethods()
  api.post('/product', 
    koaBody(),
    async ({request, response}) => {
      await productController.createProduct(request.body)
      response.status = 201
    }
  )

  app.use(errorHandler)
  app.use(api.middleware())

  return app
}
