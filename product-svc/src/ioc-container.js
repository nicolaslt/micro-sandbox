const inversify = require("inversify");
require("reflect-metadata");

const TYPES = require('./types')

const productProducer = require('./producer')
const ProductController = require('./controller')
const config = require('./config')
const applicationFactory = require('./application')
const kafkaProducer = require('./kafka-producer')

const container = new inversify.Container()
container.bind(TYPES.Config).toConstantValue(config)

container.bind(TYPES.KafkaProducer).toDynamicValue(kafkaProducer).inSingletonScope()
container.bind(TYPES.ProductDBFactory).toFactory(productProducer.factory)
container.bind(TYPES.ProductDB).toDynamicValue(({container}) => {
  const config = container.get(TYPES.Config)
  const factory = container.get(TYPES.ProductDBFactory)
  return factory(config.kafka)
}).inSingletonScope()
container.bind(TYPES.ProductController).to(ProductController)

container.bind(TYPES.Application).toDynamicValue(applicationFactory).inSingletonScope()

module.exports = container

