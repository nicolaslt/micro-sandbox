const container = require('./ioc-container')
const TYPES = require('./types')
const application = container.get(TYPES.Application)

application.listen(80)
