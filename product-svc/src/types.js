module.exports = {
  KafkaProducer: Symbol.for('KafkaProducer'),
  ProductDB: Symbol.for('ProductDB'),
  ProductDBFactory: Symbol.for('ProductDBFactory'),
  ProductController: Symbol.for('ProductController'),
  Config: Symbol.for('Config'),
  Application: Symbol.for('Application')
}
