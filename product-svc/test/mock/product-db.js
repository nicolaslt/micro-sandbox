const { ProductProducer } = require('../../src/producer')

class Producer extends ProductProducer {
  constructor(...args) {
    super(...args)
    this.queue = []
  }

  async productCreated(product) {
    this.queue.push(product)
  }
}

module.exports = {
  factory: ({ TOPIC }) => new Producer(TOPIC)
}
