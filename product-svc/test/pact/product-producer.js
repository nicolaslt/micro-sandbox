const path = require('path')
const { MessageProviderPact, Message } = require("@pact-foundation/pact");
const uuid = require('uuid')

describe("Message provider tests", () => {

  const p = new MessageProviderPact({
    messageProviders: {
      /* TODO somehow hook that up to the controller unit tests so that changing the unit tests makes the pact fail
       * this should also remove the need to create a uuid in this test as the controller does it
       */
      "a created product": () => ({ id: 2, _id: uuid.v4(), title: 'is a test dw'})
    },
    provider: "ProductProducer",
    providerVersion: "1.0.0",
    pactUrls: [
      path.resolve(__dirname, "..", "..", "..", "pacts", "elasticsearchriver-productproducer.json")
    ],
  });

  describe("Queue consumers", () => {
    it("should be accept simple messages", () => {
      return p.verify();
    });
  });
});
