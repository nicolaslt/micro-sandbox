const http = require('http')
const request = require('supertest')
const chai = require('chai')
const { expect } = chai

const container = require('./ioc-container.js')
const TYPES = require('../../src/types.js')

describe('Product application', () => {
  const app = http.createServer(container.get(TYPES.Application).callback())

  it("should throw a 400 on invalid data", () => {
    return request(app)
      .post('/api/product')
      .send({})
      .expect(400)
  })

  it("parses JSON and adds a valid product to the queue", async () => {
    const id = Date.now()
    const productDB = container.get(TYPES.ProductDB)
    await request(app)
      .post('/api/product')
      .send({id, title: 'http test'})
      .expect(201)

    expect(productDB.queue.some(p => p.id == id)).to.be.true
  })
})
