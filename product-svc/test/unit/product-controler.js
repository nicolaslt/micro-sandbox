const chai = require('chai')
const chaiAsPromised = require("chai-as-promised")
chai.use(chaiAsPromised)
const { expect } = chai

const container = require('./ioc-container.js')
const TYPES = require('../../src/types.js')
const { InvalidDataError } = require('../../src/errors')
const baseProduct = {
  title: 'a product'
}

// TODO something hangs around after the tests, probably need to _not_ try to connect to Kafka ? (refactor producer into async provider)

describe('Product controller', () => {
  const controller = container.get(TYPES.ProductController)
  const productDB = container.get(TYPES.ProductDB)

  it('adds a product to the queue', async () => {
    // This doesn't really "tie" the tests with the actual implementation, the mock db could very well change and we wouldn't be wiser but that'll do for now
    const product = {
      ...baseProduct,
      id: 1234
    }
    await controller.createProduct(product)
    expect(productDB.queue).to.include(product)
  })

  it('throws when no title is provided', async () => {
    return expect(controller.createProduct({})).to.eventually.be.rejectedWith(InvalidDataError)
  })

  it('sets an _id to the product', async () => {
    // This doesn't really "tie" the tests with the actual implementation, the mock db could very well change and we wouldn't be wiser but that'll do for now
    const product = {
      ...baseProduct,
      _id: 'overrideMe',
      key: '_idTest'
    }
    await controller.createProduct({...product})
    const added =  productDB.queue.find(p => p.key == '_idTest')
    expect(added._id).to.be.ok.and.not.equal(product._id)
  })
})
