const inversify = require("inversify");

const baseContainer = require('../../src/ioc-container')

const TYPES = require('../../src/types.js')
const productProducer = require('../mock/product-db')

const container = new inversify.Container()
container.bind(TYPES.ProductDBFactory).toFactory(context => productProducer.factory)

container.parent = baseContainer

module.exports = container
