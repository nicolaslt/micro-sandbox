const Kafka = require('node-rdkafka')
const chai = require('chai')
const { expect } = chai
const { promisify } = require('es6-promisify')

const container = require('../../src/ioc-container.js')
const TYPES = require('../../src/types.js')
const makeProducer = container.get(TYPES.ProductDBFactory)
const { address } = container.get(TYPES.Config).kafka

const TOPIC = `product.${Date.now()}`
let producer
async  function createTopicAndProducer() {
  this.timeout(10000)
  const client = Kafka.AdminClient.create({
    'client.id': 'kafka-admin',
    'metadata.broker.list': address,
  });
  await promisify(client.createTopic.bind(client))({
    topic: TOPIC,
    num_partitions: 1,
    replication_factor: 1
  });
  producer = await makeProducer({ TOPIC })
}

const consumers = []
function newConsumer() {

  return new Promise((resolve, reject) => {
    try {
      const consumer = new Kafka.KafkaConsumer({
        'group.id': Date.now(),
        'metadata.broker.list': address,
        //'queued.min.messages': 1,
        //'consume.callback.max.messages': 1,
        //'fetch.wait.max.ms': 0,
        //debug: 'all'
      }, {
        'auto.offset.reset': 'earliest',
      });
      consumers.push(consumer)
      consumer.on('ready', () => {
        consumer.subscribe([TOPIC]);
        resolve(consumer)
      })
      consumer.on('event.error', err => {
        reject(err)
      })
      consumer.on('event.log', log => console.log(`consumer: event log: ${JSON.stringify(log)}`))

      consumer.connect()
    } catch (err) {
      reject(err)
    }
  })
}

describe('Producer', () => {
  before(createTopicAndProducer)
  after(() => {
    for (consumer of consumers) {
      consumer.disconnect()
    }
  })

  it('appends a product-added message to the queue', async function () {
    this.timeout(5000)
    const consumer = await newConsumer()
    const product = {id: 'someduct'}
    await producer.productCreated(product)
    const message = await promisify(cb => consumer.consume(cb))()
    expect(message.value.toString()).to.equal(JSON.stringify(product))
  })
})
