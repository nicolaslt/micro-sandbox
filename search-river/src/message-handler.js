const inversify = require('inversify')
const TYPES = require('./types')

class MessageHandler {
  constructor (ESIndex) {
    this.index = ESIndex
  }

  async handle(product) {
    return this.index.indexProduct(product)
  }
}

inversify.decorate(inversify.injectable(), MessageHandler)
inversify.decorate(inversify.inject(TYPES.ESIndex), MessageHandler, 0);
module.exports = MessageHandler
