module.exports = {
  kafka: {
    address: 'kafka:9092',
    topic: 'product'
  },
  elasticsearch: {
    host: 'elasticsearch:9200',
    index: 'productv1'
  }
}
