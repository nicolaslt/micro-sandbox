const inversify = require('inversify')
const TYPES = require('./types')

class ESIndex {
  constructor(es, index) {
    this.es = es
    this.index = index
    this.initialized = false
  }

  async setup() {
    await this.es.indices.create({
      index: this.index,
      body: {
        mappings: {
          product: {
            properties: {
              title: {
                type: "text"
              }
            }
          }
        }
      }
    })
  }

  async setupIfRequired()  {
    
    if (this.initialized) {
      return
    }

    const indexExists = await this.es.indices.exists({index: this.index})
    if (!indexExists) {
      await this.setup()
    }
    this.initialized = true
  }

  async indexProduct(product) {
    await this.setupIfRequired()
    await this.es.index({
      index: this.index,
      type: 'product',
      id: product._id,
      body: {
        ...product,
        _id: undefined
      }
    })
  }
}

inversify.decorate(inversify.injectable(), ESIndex)
inversify.decorate(inversify.inject(TYPES.ES), ESIndex, 0);

module.exports = {
  factory: ({container}) => {
    const ES = container.get(TYPES.ES)
    return index => new ESIndex(ES, index)
  },
  ESIndex
}
