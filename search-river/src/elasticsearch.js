const elasticsearch = require('elasticsearch')
const TYPES = require('./types')

module.exports = ({container}) => {
  const config = container.get(TYPES.Config)
  return new elasticsearch.Client(config.elasticsearch)
}
