const inversify = require("inversify");
require("reflect-metadata");

const TYPES = require('./types')

const config = require('./config')
const kafkaConsumer = require('./kafka-consumer')
const elasticsearch = require('./elasticsearch')
const ESIndex = require('./es-index')
const Handler = require('./message-handler')

const container = new inversify.Container()

container.bind(TYPES.Config).toConstantValue(config)
container.bind(TYPES.ES).toDynamicValue(elasticsearch).inSingletonScope()
container.bind(TYPES.ESIndexFactory).toFactory(ESIndex.factory)
container.bind(TYPES.ESIndex).toDynamicValue(({container}) => {
  const factory = container.get(TYPES.ESIndexFactory)
  const { index } = container.get(TYPES.Config).elasticsearch
  return factory(index)
})

.inSingletonScope()
container.bind(TYPES.MessageHandler).to(Handler).inSingletonScope()
container.bind(TYPES.kafkaConsumerFactory).toFactory(kafkaConsumer)
container.bind(TYPES.kafkaConsumer).toDynamicValue(({container}) => {
  const config = container.get(TYPES.Config)
  const factory = container.get(TYPES.kafkaConsumerFactory)
  const handler = container.get(TYPES.MessageHandler)
  return factory(handler, config.kafka.topic)
})

module.exports = container

