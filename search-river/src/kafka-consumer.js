const inversify = require('inversify')
const Kafka = require('node-rdkafka')

const TYPES = require('./types')

module.exports = ({container}) => {
  const { address } = container.get(TYPES.Config).kafka
  return (handler, topic) => {

    const stream = Kafka.KafkaConsumer.createReadStream({
      'group.id': 'search-river',
      'metadata.broker.list': address,
      //'queued.min.messages': 1,
      //'consume.callback.max.messages': 1,
      //'fetch.wait.max.ms': 0,
      //debug: 'all'
    }, {
      'auto.offset.reset': 'earliest',
    }, {
      topics: [topic]
    });
    stream.on('data', message => {
      handler.handle(JSON.parse(message.value))
      // TODO need to explicitely commit?
    })
    stream.connect()
    return stream
  }
}
