module.exports = {
  Config: Symbol.for('Config'),
  kafkaConsumer: Symbol.for('kafkaConsumer'),
  kafkaConsumerFactory: Symbol.for('kafkaConsumerFactory'),
  MessageHandler: Symbol.for('MessageHandler'),
  ES: Symbol.for('ES'),
  ESIndex: Symbol.for('ESIndex'),
  ESIndexFactory: Symbol.for('ESIndexFactory'),
}
