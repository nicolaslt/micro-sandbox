const Kafka = require('node-rdkafka')
const chai = require('chai')
const { expect } = chai
const { promisify } = require('es6-promisify')

const container = require('../../src/ioc-container.js')
const TYPES = require('../../src/types.js')
const makeConsumer = container.get(TYPES.kafkaConsumerFactory)
const { address } = container.get(TYPES.Config).kafka

const TOPIC = `product.${Date.now()}`
const product = {id: 'someduct'}

async function createTopicWithAMessage() {
  this.timeout(10000)
  const client = Kafka.AdminClient.create({
    'client.id': 'kafka-admin',
    'metadata.broker.list': address,
  });
  await promisify(client.createTopic.bind(client))({
    topic: TOPIC,
    num_partitions: 1,
    replication_factor: 1
  });
  const producer = Kafka.Producer({
    'metadata.broker.list': address,
    'queue.buffering.max.ms': 10,
    'queue.buffering.max.messages': 1,
    'message.timeout.ms': 1,
    'event_cb': true,
    'dr_cb': true,
    debug: 'all'
  })
  return new Promise((resolve, reject) => {
    producer.on('ready', async () => {
      producer.produce(TOPIC, null, Buffer.from(JSON.stringify(product)))
      producer.poll()
      
      //await promisify(cb => producer.flush(2000, cb))()
      resolve(producer)
    })
    producer.connect()
  })
}

describe('Consumer', () => {
  before(createTopicWithAMessage)

  it("is able to read messages from the queue", async function () {
    this.timeout(30000)
    return new Promise((resolve, reject) => {
      try {
        const stream = makeConsumer(message => {
          try {
            expect(JSON.stringify(message)).to.equal(JSON.stringify(product))
            resolve()
          } catch (err) {
            reject(err)
          }
        },TOPIC)
      } catch (err) {
        reject(err)
      }
    })
  })
})
