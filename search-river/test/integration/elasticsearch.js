const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const { expect } = chai
const container = require('../../src/ioc-container.js')
const TYPES = require('../../src/types.js')

const es = container.get(TYPES.ES)
const makeESIndex = container.get(TYPES.ESIndexFactory)

describe('Elasticsearch', () => {
  it('is connected', async () => {
    await es.ping()
  })

  describe('index', () => {
    it("builds", () => {
      expect(makeESIndex()).to.be.ok
    })

    it("creates an index if necessary", async () => {
      const indexName = Date.now().toString()
      const index = makeESIndex(indexName)

      await index.setup()
      // This will throw if index does not exists
      const indexConf = (await es.indices.get({index: indexName}))[indexName]
      expect(indexConf.mappings).to.not.be.empty
    })

    it("is able to index a product", async () => {
      const indexName = Date.now().toString()
      const index = makeESIndex(indexName)

      await index.setup()
      await index.indexProduct({title: 'Hullo tester', _id: 'testhullo'})
      const indexed = await es.get({index: indexName, type: 'product', id:'testhullo'})
      expect(indexed).to.be.ok
    })

    it("creates an index automatically if doesn't exist when indexing something", async () => {
      const indexName = Date.now().toString()
      const index = makeESIndex(indexName)

      await index.indexProduct({title: 'Hullo tester', _id: 'testhullo'})
      // This will throw if index does not exists
      const indexConf = (await es.indices.get({index: indexName}))[indexName]
      // Try to detect our own mapping instead of the dynamically created one
      expect(indexConf.mappings.product.properties.title).to.deep.equal({type: 'text'})
    })
  })
})
