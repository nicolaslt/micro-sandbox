const path = require('path')
const { Message, MessageConsumerPact, Matchers } = require('@pact-foundation/pact')

const { like, uuid } = Matchers

// TODO write a real one, though need to understand what Pact expects it to be
// so we can check whether this need to be ran agains a real implementation or just a mock
async function messageHandler() {}

const messagePact = new MessageConsumerPact({
  consumer: "ElasticSearchRiver",
  dir: path.resolve(__dirname, "..", "..", "pacts"),
  pactfileWriteMode: "update",
  provider: "ProductProducer",
})

describe("receive a product event", () => {
  it("should accept a product event", () => {
    return messagePact
      // .given("some state")
      .expectsToReceive("a created product")
      .withContent({
        id: like(1),
        _id: uuid(),
        title: like("rover")
      })
      .withMetadata({
        "content-type": "application/json",
      })
      .verify(messageHandler)
  })
})
