# Microthings sandbox

A monorepository where I will be exploring microservices, event sourcing and micro-frontends.

Unless my sofa has the better of me, or work takes my energy too much or some other excuse.

# The plan

Make a naively basic product & basket page combo. This shall not be deployed anywhere, to avoid loosing time learning the latest AWS/GCP offering.

Use the following technology:
- Kafka for event sourcing
- Node.JS for all services
- Elasticsearch for listing products
- Pact.io for contract testing, initially only to test message consumer/producer contract, but maybe use it for backend/frontend?.
- React or Vue (or both) for the frontend, using Web Components and composition techniques described on [micro-frontends.org](https://micro-frontends.org/)


# Roadmap

## v1

Make a system able to hold product (search) and allows adding new ones

- [x] Product Kafka queue (to hold anything about products data domain)
- [x] Product Service.
  - Runs input validation.
  - Posts messages to the product queue: 'Product Added'
- [x] Search river
  - Subscribes to product queue 'Product Added' and add to the ES index
- [ ] Search service
  - Simple http service querying elasticsearch
- [ ] API gateway. For now synchronous with client side to limit the amount of learning I need to do.
  - HTTP PUT to add product
  - HTTP GET/POST to search products

## v2

Add front-end to v1

- Micro front-end SPA able to show:
  - [ ] A list of products (handled by the "search team")
  - [ ] An "Create product" button+form (handled by the "product team")

## v3

Have fun with eventual consistency through local strong consistency, by using SKU unicity as a problem space

- [ ] Product service posts a Saga instead of "product created". For simplicity, product service itself would be the orchestrator (which I think might be an anti-pattern), such as:
  - T1: reserve SKU
  - C1: release SKU
  - Success: "Product Created"
  - Error: "Product failed because reason"

- FE:
  - [ ] have a notification area to consume and ACK those errors, still synchronous (refresh needed to see the notification)
 
- ES:
  - [ ] subscribe to new "Product failed" error and indexes them
  - [ ] subscribe to error ACK and remove from index
- SKU service:
  - [ ] PGSQL with a single table (SKU, msg id), SKU primary+unique
  - [ ] Subscribes to "Reserve SKU"
    - Reply either OK/Nope
  - [ ] Subscribes to "Release SKU"
    - Reply either OK (if exists -> released, if not exists -> de facto released, if system error -> consumer position not updated so equivalent to not doing anything- though on a production system there may be a queue for the system error so it can be surfaced by monitoring/logging)


## Interlude

Look into CQRS a bit more and decide whether to use Server Sent Event (or websockets or even polling) to avoid needing to refresh the client side. This might lead to maybe creating a Kafka topic dedicated to web communication, maybe per client? Kinda depends on how to subscribe to topics and how it all fits together with the app model.

## Later

Look into adding a basket and a naive stock management.
